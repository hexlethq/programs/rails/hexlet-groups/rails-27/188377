# BEGIN
def make_censored(text, stop_words)
  stroke_text = text.split(' ')
  stop_text = stop_words
  censor = '$#%!'
  censor_text = []
  stroke_text.each do |stroke|
    compare = stop_text.include?(stroke)
    compare ? (censor_text << censor) : (censor_text << stroke)
  end
  censor_text.join(' ')
end
# END
