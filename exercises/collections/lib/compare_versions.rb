# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  version_array1 = version1.split('.')
  version_array2 = version2.split('.')

  comp_version1 = (version_array1[0].to_i <=> version_array2[0].to_i)
  comp_version2 = (version_array1[1].to_i <=> version_array2[1].to_i)
  comp_version1.zero? ? comp_version2 : comp_version1
end
# END

