
# BEGIN
def build_query_string(hash)
  hashsort = hash.sort
  query_string = []
  hashsort.each { |key, value| query_string << "#{key}=#{value}" }
  query_string.join('&')
end
# END