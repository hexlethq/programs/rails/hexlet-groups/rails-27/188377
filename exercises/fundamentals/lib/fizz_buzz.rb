# frozen_string_literal: true

# BEGIN
def get_string_value(number)
  if (number % 5).zero? && (number % 3).zero?
    'FizzBuzz'
  elsif (number % 3).zero?
    'Fizz'
  elsif (number % 5).zero?
    'Buzz'
  else
    number.to_s
  end
end

def fizz_buzz(start, stop)
  arr_res = []
  if start > stop
    nil
  else
    (start..stop).each do |number|
      arr_res.push(get_string_value(number))
    end
    arr_res.join(' ')
  end
end
# END
