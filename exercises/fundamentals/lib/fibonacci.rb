# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  return nil if num <= 0
  return num - 1 if num <= 2

  fibonacci(num - 1) + fibonacci(num - 2)
end
# END
