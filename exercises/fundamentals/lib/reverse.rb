# frozen_string_literal: true

# BEGIN
def reverse(str)
  s = []
  str.each_char do |x|
    s.unshift(x)
  end
  s.join
end
# END
