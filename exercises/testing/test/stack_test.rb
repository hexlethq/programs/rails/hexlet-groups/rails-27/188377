# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'
require 'minitest'
require 'minitest/power_assert'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
  end

  def test_stack_new_array
    assert { @stack.to_a == [] }
  end

  def test_stack_push
    @stack.push!('ruby')
    @stack.push!('php')
    @stack.push!('java')

    assert { @stack.to_a == %w[ruby php java] }
  end

  def test_pop_empty_stack
    assert { @stack.pop!.nil? }
  end

  def test_pop
    @stack = Stack.new(%w[ruby php java])
    popped = @stack.pop!

    assert { popped == 'java' }
    assert { @stack.to_a == %w[ruby php] }
  end

  def test_stack_clear
    @stack = Stack.new(%w[ruby php java])
    @stack.clear!

    assert { @stack.to_a == [] }
  end

  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
