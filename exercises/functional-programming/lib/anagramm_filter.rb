# frozen_string_literal: true

# BEGIN
def anagramm_filter(word, word_array)
  sorted_word = word.chars.sort
  word_array.filter { |item| item.chars.sort == sorted_word }
end
# END
