# frozen_string_literal: true

# BEGIN
def get_same_parity(args)
  return args if args.empty?

  args.first.even? ? args.select(&:even?) : args.select(&:odd?)
end
# END
