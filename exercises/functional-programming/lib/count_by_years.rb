# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  male_users = users.filter { |hash| hash[:gender] == 'male' }
  male_users.each_with_object(Hash.new(0)) do |male_hash, birthday_hash|
    birthday_hash[male_hash[:birthday].slice(0..3)] += 1
  end
end
# END
