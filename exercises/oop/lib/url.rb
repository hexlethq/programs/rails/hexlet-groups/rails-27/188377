# frozen_string_literal: true

# BEGIN
require 'forwardable'
require 'uri'

class Url
  extend Forwardable
  include Comparable

  def initialize(url)
    @adress = URI(url)
  end

  def_delegators :@adress, :host, :scheme, :to_s

  def query_params
    @query_params ||= @adress.query.nil? ? 'Adress is nil' : build_query_params
  end

  private

  def build_query_params
    @adress.query.split('&').each_with_object({}) do |arr, hash|
      tmp = arr.split('=')
      hash[tmp[0].to_sym] = tmp[1]
    end
  end

  public

  def query_param(key, value = nil)
    query_params[key] ||= value
  end

  def <=>(other)
    @adress.to_s <=> other.to_s
  end
end

# END
